% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined
% with member(?Elem,?List)
reaching(Graph,Node,List) :- findall(OutTerm,member(e(Node,OutTerm),Graph),List).
