% fromCircList(+List,-Graph)
fromCircList([H|T],Graph) :- fromCircList2(H,[H|T],Graph).
fromCircList2(H,[X],[e(X,H)]).
fromCircList2(H,[H1,H2|T],[e(H1,H2)|L]) :- fromCircList2(H,[H2|T],L).