% dropFirst(?Elem,?List,?OutList)
dropFirst(X,[X|T],T):- !.
dropFirst(X,[H|T],[H|L]) :- dropFirst(X,T,L).

% dropLast(?Elem,?List,?OutList)
dropLast(X,List,OutList):- reverse(List,ListR), dropFirst(X,ListR,OutListR), reverse(OutListR,OutList).

% dropAll(?Elem,?List,?OutList)
dropAll(_,[],[]).
dropAll(E,[H|T],Result) :- H=E, dropAll(E,T,Result),!.
dropAll(E,[H|T],[H|Result]) :- dropAll(E,T,Result).