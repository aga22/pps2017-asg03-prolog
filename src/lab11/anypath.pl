% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath(Graph,N1,N2,[e(N1,N2)]) :- member(e(N1,N2),Graph).
anypath(Graph,N1,N2,[e(N1,N3)|List]) :- member(e(N1,N3),Graph),anypath(Graph,N3,N2,List).

