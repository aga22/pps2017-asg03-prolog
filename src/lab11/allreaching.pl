% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!
anypath(Graph,N1,N2,[e(N1,N2)]) :- member(e(N1,N2),Graph).
anypath(Graph,N1,N2,[e(N1,N3)|List]) :- member(e(N1,N3),Graph),anypath(Graph,N3,N2,List).
allreaching(Graph,N,List) :- findall(OutTerm,anypath(Graph,N,OutTerm,_),List).
