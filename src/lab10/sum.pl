% sum(List,Sum)
% Sum all integer elements of List.
sum([],0).
sum([X|T],M) :- sum(T,N), M is X+N.