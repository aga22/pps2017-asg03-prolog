% size(List,Size)
% Size will contain the number of elements in List,
% written using notation zero, s(zero), s(s(zero)).
% can allow a pure relational behaviour
size([],zero).
size([_|T],s(M)) :- size(T,M).