% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element
max([H|T],Max) :- max2(T,Max,H),!.
max2([H|T],Max,TempMax) :- H>TempMax,max2(T,Max,H).
max2([H|T],Max,TempMax) :- H=<TempMax,max2(T,Max,TempMax).
max2([],Max,Max).
