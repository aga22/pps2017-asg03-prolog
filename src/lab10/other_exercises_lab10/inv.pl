% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
% inv metter in relazione due liste di cui la seconda è l'inverso della prima
% caso base --> le due liste sono vuote
% passo ricorsione --> richiamo inv sulla coda della prima e uso la append/3
% con cui concateno la testa sulla coda già invertita
% inv([],[]).
% inv([H|T],ListReversed) :- inv(T,TReversed),append(TReversed,[H],ListReversed).

inv([],List,List) :- !.
inv([H|T],TempList,Reversed) :- inv(T,[H|TempList],Reversed).
inv(List,Reversed) :- inv(List,[],Reversed).