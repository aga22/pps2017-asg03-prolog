% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
% Mantengo il riferimento alla lista intermedia che si crea durante la ricorsione
times(List,ListN,1,ListN) :- !.
times(List,N,ListN) :- times(List,List,N,ListN).
times(List,ListInt,N,ListN) :- M is N-1, M>0,append(List,ListInt,ListInt2),times(List,ListInt2,M,ListN).