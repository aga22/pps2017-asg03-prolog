% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([[H|_]],[H]) :- !.
proj([[H|_]|T], [H|T2]) :- proj(T,T2).